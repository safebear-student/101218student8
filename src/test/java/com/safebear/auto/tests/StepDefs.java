package com.safebear.auto.tests;


import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class StepDefs {

    @Given("^I navigate to the login page$")
    public void i_navigate_to_the_login_page() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }

    @When("^I enter the login details for a '(.+)'$")
    public void i_enter_the_login_details_for_a_user(String user) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }

    @Then("^I can see the following message: '(.+)'$")
    public void i_can_see_the_following_message(String message) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }

    //@When("^I enter the login details for a 'validUser'$")
    //public void i_enter_the_login_details_for_a_validUser() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
      //  throw new PendingException();
    //}

    //@Then("^I can see the following message: 'Login Successful'$")
    //public void i_can_see_the_following_message_Login_Successful() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
      //  throw new PendingException();
    //}

}
